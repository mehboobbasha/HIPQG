package com.insurance;

public class PremiumCalculator {

	public static void main(String args[]) {
		PersonDetails person = new PersonDetails();
		double basePremium = 5000;
		int habitsCount = 0;
		person.setFirstName("Norman");
		person.setLastName("Gomes");
		person.setAge(34);
		person.setGender("Male");
		person.setHyperTension(false);
		person.setBloodPressure(false);
		person.setBloodSugar(false);
		person.setOverWeight(true);
		person.setSmoking(false);
		person.setAlcohol(true);
		person.setDailyExcercise(true);
		person.setDrugs(false);
		if (person.getAge() >= 18) {
			basePremium = CallableMethods.addPercentage(basePremium, 10);
		}
		if (person.getAge() >= 25) {
			basePremium = CallableMethods.addPercentage(basePremium, 10);
		}
		if (person.getAge() >= 30) {
			basePremium = CallableMethods.addPercentage(basePremium, 10);
		}
		if (person.getAge() >= 35) {
			basePremium = CallableMethods.addPercentage(basePremium, 10);
		}
		if (person.getAge() >= 40) {
			int age = person.getAge() - 40;

			basePremium = CallableMethods.addPercentage(basePremium, 20);

			while (age >= 5) {
				basePremium = CallableMethods.addPercentage(basePremium, 20);
				age -= 5;

			}

		}

		if (person.getGender().toLowerCase().equals("male")) {
			basePremium = CallableMethods.addPercentage(basePremium, 2);
		}

		if (person.isHyperTension() == true) {
			basePremium = CallableMethods.addPercentage(basePremium, 1);
			System.out.println("base premium till Hypertension== " + basePremium);
		} else {
			basePremium = CallableMethods.addPercentage(basePremium, 0);
		}

		if (person.isBloodPressure()) {
			basePremium = CallableMethods.addPercentage(basePremium, 1);
		}
		if (person.isBloodSugar()) {
			basePremium = CallableMethods.addPercentage(basePremium, 1);
		}
		if (person.isOverWeight()) {
			basePremium = CallableMethods.addPercentage(basePremium, 1);
		}

		if (person.isSmoking()) {
			habitsCount++;
		}
		if (person.isAlcohol()) {
			habitsCount++;
		}
		if (person.isDrugs()) {
			habitsCount++;
		}
		if (person.isDailyExcercise()) {
			habitsCount--;
		}
		if (habitsCount < 0) {
			basePremium = CallableMethods.subPercentage(basePremium, 3);
		} else {
			while (habitsCount > 0) {
				basePremium = CallableMethods.addPercentage(basePremium, 3);
				habitsCount--;
			}
		}
		System.out
				.println("Health Insurance Premium for Mr." + person.getLastName() + " RS:" + Math.round(basePremium));

	}

}
