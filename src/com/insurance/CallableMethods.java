package com.insurance;

public class CallableMethods {

	public static double addPercentage(double base, int percentage) {
		double res = base + ((base * percentage) / 100);
		return res;
	}

	public static double subPercentage(double base, int percentage) {
		double res = base - ((base * percentage) / 100);
		return res;
	}
}
